using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class DisapearComponent : FindComponent 
{
    [SerializeField] private float _disapearDuration;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public override void DoEffect(Action callback)
    {
        _spriteRenderer.DOFade(0f, _disapearDuration).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        });
    }
}
