using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class Level : MonoBehaviour
{
    private List<GameItem> _items = new List<GameItem>();
    private int _itemsCount;

    public event Action OnComplete;
    public event Action<string> OnItemListChanged;
    
    public void Initialize()
    {
        _items.AddRange(GetComponentsInChildren<GameItem>());
        _itemsCount = _items.Count;

        for (int i=0;i<_items.Count; i++)
        {
            _items[i].OnFind += OnItemfinded ;
        }
    }

    public Dictionary <string, GameItemData > GetItemDictionary()
    {
        Dictionary<string, GameItemData> dictionary = new Dictionary<string, GameItemData>();

        for(int i=0; i<_itemsCount; i++)
        {
            if(dictionary.ContainsKey(_items [i].Name)==true)
            {
                dictionary[_items[i].Name].IncreaseCount();
            }
            else
            {
                dictionary.Add(_items[i].Name, new GameItemData (_items[i].Sprite));
            }
        }
        return dictionary;

    }

    private void OnItemfinded(string name)
    {
        _itemsCount--;
        OnItemListChanged?.Invoke(name);

        if(_itemsCount==0)
        {
            OnComplete?.Invoke(); 
        }
    }
}
