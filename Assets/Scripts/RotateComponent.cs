using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class RotateComponent : FindComponent 
{
    [SerializeField] private float _rotateDuration;
    public override void DoEffect(Action callback)
    {
        transform.DORotate (new Vector3 (0f,0f,360), _rotateDuration, RotateMode.FastBeyond360).OnComplete(() =>
        {
            gameObject.SetActive(false);
            callback?.Invoke();
        });
    }
}
